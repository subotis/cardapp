class CreateCards < ActiveRecord::Migration[5.2]
  def change
    create_table :cards do |t|

      t.integer :suit
      t.string :card, limit: 5
      t.string :status
      t.string :issuer_ip

      t.timestamps
    end
  end
end
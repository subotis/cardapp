class CreateLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :logs do |t|
      t.string :kind
      t.string :amount

      t.timestamps
    end
  end
end

require 'database_cleaner'

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean
if ActiveRecord::Base.connection.table_exists? 'cards'
  Card.suits.keys.each do |s|
    %w[2 3 4 5 6 7 8 9 10 11 12 13 a].each do |c|
      Card.create(suit: s, card: c)
    end
  end
  Card.create(card: 'joker')
  RestockJob.set(wait: 1.hour).perform_later
end

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  namespace :api, defaults: { format: :json } do
    get 'card', to: 'card#get', format: false
    put 'card', to: 'card#put', format: false
    namespace :admin do
      get 'stock', to: 'admin#stock', format: false
      get 'finances', to: 'admin#finances', format: false
    end
  end
end

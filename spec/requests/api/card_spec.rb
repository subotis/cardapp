require 'rails_helper'

describe "get card", :type => :request do

  before { get '/api/card' }

  it 'returns card suit' do
    expect(%w[spade heart club diamond]).to include JSON.parse(response.body)['suit']
  end
  it 'returns card' do
    expect(%w[2 3 4 5 6 7 8 9 10 11 12 13 a joker]).to include JSON.parse(response.body)['card']
  end

  it 'returns status code 200' do
    expect(response).to have_http_status(:success)
  end
end
describe "return card", :type => :request do

  it 'returns card' do
    get '/api/card'
    r = JSON.parse(response.body)
    put '/api/card', params: { suit: r['suit'], card: r['card'] }
  end
  it 'returns status code 200' do
    get '/api/card'
    r = JSON.parse(response.body)
    put '/api/card', params: { suit: r['suit'], card: r['card'] }
    expect(response).to have_http_status(:success)
  end

end

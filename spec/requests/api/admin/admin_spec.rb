require 'rails_helper'

describe "get stock", :type => :request do

  before { get '/api/admin/stock' }

  it 'returns card suit' do
    expect(JSON.parse(response.body)['available_cards']).to eq(53)
  end

  it 'returns status code 200' do
    expect(response).to have_http_status(:success)
  end
end
describe "stock after rent", :type => :request do

  it 'returns stock' do
    get '/api/card'
    get '/api/admin/stock'
    expect(JSON.parse(response.body)['available_cards']).to eq(52)
    expect(JSON.parse(response.body)['rented_cards']).to eq(1)
    expect(response).to have_http_status(:success)
  end
  it 'updates stock' do
    get '/api/card'
    r = JSON.parse(response.body)
    get '/api/admin/stock'
    expect(JSON.parse(response.body)['available_cards']).to eq(52)
    expect(JSON.parse(response.body)['rented_cards']).to eq(1)
    put '/api/card', params: { suit: r['suit'], card: r['card'] }
    get '/api/admin/stock'
    expect(JSON.parse(response.body)['available_cards']).to eq(53)
    expect(response).to have_http_status(:success)
  end

end


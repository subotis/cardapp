class Card < ApplicationRecord
  enum suit: %i[spade heart club diamond]
  TIME_TO_RETURN = 15 * 60 #in seconds
  include AASM

  aasm column: 'status' do

    state :stock, initial: true
    state :issued
    state :lost

    event :issued do
      transitions from: :stock, to: :issued
    end
    event :lost do
      transitions from: :issued, to: :lost
    end
    event :stock do
      transitions from: %i[issued lost], to: :stock
    end

  end

  def make_issued(client_ip)
    update_attribute(:issuer_ip, client_ip)
    issued!
    touch
    CardJob.perform_in(TIME_TO_RETURN, self.id)
  end

  def make_lost
    lost!
  end

  def restock!
    Log.create(kind: 'card_replacement', amount: '0.5')
    Finance.restock_card
    stock!
  end

  def make_stock
    Log.create(kind: 'rent', amount: Finance.retrieve_rent_amount(self)) # need refactoring
    Finance.retrieve_rent(self)
    stock!
  end
end

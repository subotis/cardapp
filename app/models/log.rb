class Log < ApplicationRecord
  def self.last_3_hours
    Log.where('created_at > ?', 3.hours.ago)
        .order(created_at: :desc)
        .pluck(:kind, :amount, :created_at)
        .map { |kind, amount, created_at| { type: kind,
                                            amount: amount.to_f / 100,
                                            created_at: created_at.to_formatted_s(:iso8601)
                                          }
        }
  end
end

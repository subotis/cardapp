# frozen_string_literal: true

class Finance
  @@balance = 500

  class << self
        def restock_card
          @@balance -= 50
          exec 'kill $(cat tmp/pids/server.pid)' if @@balance.negative?
        end

        def balance
          format('%.2f', (@@balance / 100.0))
        end

        def retrieve_rent(card)
          @@balance += retrieve_rent_amount(card)
        end

        def retrieve_rent_amount(card)
          round_time(card)
        end

        def pending_rent
          cards = Card.where(status: 'issued')
          amount = 0
          cards.each do |c|
            amount += round_time(c)
          end
          amount.to_f / 100 # make pretty output
        end

        def pending_replacement
          cards = Card.where(status: 'lost')
          amount = 0
          cards.each { amount += 50 }
          amount.to_f / 100 # make pretty output
        end

    private

        def round_time(card)
          ((Time.now - card.updated_at).to_f / 60).ceil
        end
  end
end

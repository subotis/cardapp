class RestockJob < ApplicationJob
  self.queue_adapter = :sucker_punch
  RUN_EVERY = 1.hour
  before_perform :enqueue_again

  def perform
    Card.where(status: 'lost').each do |c|
      c.restock!
      c.update_columns issuer_ip: nil
    end
  end

  private

  def enqueue_again
    RestockJob.set(wait: RUN_EVERY).perform_later
  end


end
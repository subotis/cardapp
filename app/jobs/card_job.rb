class CardJob
  include SuckerPunch::Job

  def perform(id)
    card = Card.find(id)
    Ban.where(ip: card.issuer_ip).first_or_create if card.make_lost if card.issued?
  end
end
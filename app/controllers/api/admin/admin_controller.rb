# frozen_string_literal: true

class Api::Admin::AdminController < ApplicationController
  KEY_MAPPER = {
    'stock' => 'available_cards',
    'issued' => 'rented_cards',
    'lost' => 'lost_cards'
  }.freeze

  def stock
    result = Card.group(:status).count(:status).map { |k, v| [KEY_MAPPER[k], v] }.to_h
    render json: result.to_json(only: %w[available_cards rented_cards lost_cards]), status: :ok
  end

  def finances
    result = {
      balance: Finance.balance,
      pending_rent: Finance.pending_rent,
      pending_replacement: Finance.pending_replacement,
      recent_transactions: Log.last_3_hours
    }
    render json: result.to_json, status: :ok
  end
end

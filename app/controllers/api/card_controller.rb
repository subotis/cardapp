# frozen_string_literal: true

class Api::CardController < ApplicationController
  before_action :check_for_ban, only: [:get]

  def get
    @card = Card.where(status: :stock).sample
    if @card
      @card.make_issued(request.remote_ip)
      render json: @card.to_json(only: %i[suit card]), status: :ok
    else
      render json: { error: 'There is no available cards' }, status: :not_found
    end
  end

  def put
    @card = Card.find_by_card_and_suit(params['card'], params['suit'])
    if @card
      if @card.issued?
        @card.make_stock
        render json: @card.to_json(only: %i[suit card]), status: :ok
      else
        render json: { notice: 'This card cannot be returned' },
               status: :forbidden
      end
      # maybe make some check to ensure that issued client returns card?
    else
      render json: { error: 'Card not found' }, status: :not_found
    end
  end

  private

  def check_for_ban
    if Ban.exists?(ip: request.remote_ip)
      render json: { notice: 'You are banned from retrieving new card' },
             status: :forbidden
    end
  end
end
